import { Router, Request, Response, json } from 'express';
import { Model, Mongoose } from 'mongoose';
import { IPool } from '../../definitions/pools';
import { PoolSchema } from '../mongoose/schemas/pool.schema';

export class Authcontroller {

    private connection: Mongoose;
    private poolModel: Model<IPool>;

    public constructor(connection: Mongoose) {
        this.connection = connection;
        this.poolModel = this.connection.model("Pool", PoolSchema);
    }

    buildRoutes(): Router {
        const router = Router();
        // router.post("/create", json(), this.newPool.bind(this));
        // router.get("/:id", this.getOne.bind(this));
        // router.get("/", this.getAll.bind(this));
        return router;
    }

}