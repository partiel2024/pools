import { Mongoose } from "mongoose";
import { Authcontroller } from "./pool.controller";
import * as express from "express";

export function launchAPI(connection: Mongoose) {
    const app = express();
    const authController = new Authcontroller(connection);
    app.use("/", authController.buildRoutes());
    app.listen(process.env.PORT, ()=>{
        console.log(`API listening on port ${process.env.PORT}`);
    })
}