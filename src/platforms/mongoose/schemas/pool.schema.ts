import mongoose, { SchemaTypes } from "mongoose";
import { IPool } from "../../../definitions/pools";

export const PoolSchema = new mongoose.Schema<IPool>({
    tokenA: {
        type: SchemaTypes.String,
        required: true
    },
    tokenB: {
        type: SchemaTypes.String,
        required: true
    },
    account: {
        type: SchemaTypes.ObjectId,
        required: true,
        ref: "Account"
    }
}, {
    versionKey: false,
    collection: "sessions",
    timestamps: true
});

PoolSchema.index({ tokenA: 1, tokenB: 1 }, { unique: true });