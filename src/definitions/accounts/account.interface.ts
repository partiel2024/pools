export interface IAccount {
    username: string;
    password: string;
    privateKey: string;
    publicKey: string;
}