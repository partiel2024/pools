import { IAccount } from "../accounts";

export interface IPool {
    tokenA: string;
    tokenB: string;
    account: string | IAccount;
}